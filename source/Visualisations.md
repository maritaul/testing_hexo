## Examples

![](/hist-evol-datasets-per-repo.png)

![](/hist-quantity-year-type.png)

![](/pie--datacite-client.png) 

![](/pie--datacite-type.png)




## Sources

(so far) 

|    		|Dataset numbers      | UGA perimeter |
|-----------|---------------------|---------------|
|RDG		|42 |contact, auteurs, producteur et contributeurs avec "UGA" OR "Grenoble" |
|DataCite	|1247| creator et contributor avec ROR + clients & publisher|
|Zenodo		|1041|creator et contributor avec "grenoble"|
|Nakala		|32 |UGA user identifiers|
|BSO via HAL|32 |NA|
|... 		|   |
